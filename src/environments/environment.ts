// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
      apiKey: 'AIzaSyDBu8tizpAtj_BbztwPD86oUQyELYsxLuU',
      authDomain: 'controle-esther.firebaseapp.com',
      databaseURL: 'https://controle-esther.firebaseio.com',
      projectId: 'controle-esther',
      storageBucket: 'controle-esther.appspot.com',
      messagingSenderId: '760887099524',
      appId: '1:760887099524:web:fd9840da76759a969ac2d1',
      measurementId: 'G-VVKSW86J5E'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
