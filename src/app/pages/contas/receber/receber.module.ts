import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ReceberPageRoutingModule } from './receber-routing.module';
import { ReceberPage } from './receber.page';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ReceberPageRoutingModule
  ],
  declarations: [ReceberPage]
})
export class ReceberPageModule {}
